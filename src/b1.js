// Sự khác nhau var và const, let
// const dùng để khai báo 1 biến là hằng số mà không thay đổi giá trị trong cả project
// var dùng để khai báo biến phạm vi toàn cục, thay đổi giá trị được
// let chỉ có phạm vi sử dụng trong khối bao quanh nó, thay đổi giá trị được

const a = 10;
// a = 15;
console.log(a); // ra kết quả a = 10

var x = "Hôm nay";
console.log(x);
x = "Thứ 6";
console.log(x);

function test(){
  let m = 1;
  if(true){
    let m = 2;
    console.log(m);
  }
  console.log(m);
}
test();